# Generated by Django 2.0.6 on 2020-07-11 09:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0007_auto_20180605_2110'),
    ]

    operations = [
        migrations.AlterField(
            model_name='awardscontent',
            name='sku',
            field=models.CharField(default='LdkTbW0XzUE', help_text='Unique code for refrence to supervisors', max_length=15),
        ),
    ]
